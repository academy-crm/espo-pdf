<?php
namespace Espo\Modules\PdfEngines\Tools\Pdf\WkPdf;

use mikehaertl\wkhtmlto\Pdf;

class WkPdf extends Pdf
{
    public $binary = "wkhtmltopdf";
}